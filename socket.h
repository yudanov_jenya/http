/*
 * socket.h
 *
 *  Created on: 18 июн. 2018 г.
 *      Author: jun
 */

#ifndef SOCKET_H
#define SOCKET_H


#include <netinet/in.h>
#include <string>
#include <netdb.h>

#define BUF_SIZE 2048


class socket
{
public:
	// 			IP  	PORT
	socket(const char*,uint16_t);
	virtual bool connect();

	virtual ssize_t send(std::string);
	virtual std::string recv();

	virtual bool status();

	virtual ~socket();

protected:
	socket();
	std::string _host;
	uint16_t _port;
	struct sockaddr_in _addr;
	int _descriptsocket;
	std::string _request;
	bool _error;
};




#endif // end SOCKET_H


