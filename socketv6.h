/*
 * socket.h
 *
 *  Created on: 18 июн. 2018 г.
 *      Author: jun
 */

#ifndef SOCKETv6_H
#define SOCKETv6_H


#include <netinet/in.h>
#include <string>
#include <netdb.h>

#define BUF_SIZE 2048


class socketv6
{
public:
	// 			IP  	PORT
	socketv6(const char*,uint16_t);
	virtual bool connect();

	virtual ssize_t send(std::string);
	virtual std::string recv();

	virtual bool status();

	virtual ~socketv6();

protected:
	socketv6();
	virtual bool _connect();
	std::string _host;
	uint16_t _port;
	struct addrinfo _addr;
	struct addrinfo *_result;
	int _descriptsocket;
	std::string _request;
	bool _error;
};




#endif // end SOCKET_H


