/*
 * main.cpp
 *
 *  Created on: 18 июн. 2018 г.
 *      Author: jun
 */


#include <iostream>
#include "socket.h"
#include <string>


int main()
{
	class socket tcp("127.0.0.1",80);
	std::string message("GET /. HTTP/1.1\r\nHost: 127.0.0.1\r\n\r\n");
	ssize_t size;
	if((size = tcp.send(message)) != message.size())
		{
			if(size < 0)
				return 3;
			std::cout << "Error request size is " << message.size() << " send pulling "<< size  << std::endl;
			return 2;
		}
	std::string answer;
	if((answer = tcp.recv()) != "Error")
	{
		std::cout << answer << std::endl;
		return 0;
	}
	return 1;
}

