/*
 * ssl.h
 *
 *  Created on: 18 июн. 2018 г.
 *      Author: jun
 */

#ifndef SSL_H_
#define SSL_H_

#include "socket.h"
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>


class sslsocket : public socket
{
public:

	sslsocket(const char*, uint16_t);
	void close();
	virtual bool connect();
	virtual ssize_t send(std::string);
	virtual std::string recv();
	virtual ~sslsocket();

protected:
	sslsocket();
	SSL_CTX* _ctx;
	SSL* 	 _ssl;
};

#endif /* SSL_H_ */
