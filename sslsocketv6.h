/*
 * sslsocketv6.h
 *
 *  Created on: 20 июн. 2018 г.
 *      Author: jun
 */

#ifndef SSLSOCKETV6_H_
#define SSLSOCKETV6_H_

#include "socketv6.h"
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

class sslsocketv6 : public socketv6
{
public:
	sslsocketv6(const char*, uint16_t);

	void close();
	virtual bool connect();
	virtual ssize_t send(std::string);
	virtual std::string recv();

	virtual ~sslsocketv6();


protected:
	sslsocketv6();
	SSL_CTX* _ctx;
	SSL* 	 _ssl;
};

#endif /* SSLSOCKETV6_H_ */
