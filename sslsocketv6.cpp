/*
 * sslsocketv6.cpp
 *
 *  Created on: 20 июн. 2018 г.
 *      Author: jun
 */

#include "sslsocketv6.h"

/*
 * ssl.cpp
 *
 *  Created on: 18 июн. 2018 г.
 *      Author: jun
 */

#include "sslsocketv6.h"
#include <iostream>
#include <unistd.h>
#include <netdb.h>
#include <openssl/err.h>

sslsocketv6::sslsocketv6()
{
}

sslsocketv6::sslsocketv6(const char* host, uint16_t port)
:socketv6(host,port)
{
	SSL_library_init();
	OpenSSL_add_all_algorithms();
	SSL_load_error_strings();
	_ctx = SSL_CTX_new(SSLv23_client_method());
	if(!_ctx)
	{
		std::cerr << "Unable to create a new SSL context structure" << std::endl;
		_error = false;
	}else{
		_ssl = SSL_new(_ctx);
		if(!_ssl)
		{
			std::cerr << "Unable to create a new SSL structure for a connection" << std::endl;
			_error = false;
		} else std::cout << "Ready to connect" << std::endl;
	}

}

void sslsocketv6::close()
{
	if(_ssl)
	{
		SSL_shutdown(_ssl);
		SSL_free(_ssl);
	}
	if(_descriptsocket >=0)
		::close(_descriptsocket);
	if(_ctx)
		SSL_CTX_free(_ctx);
}

bool sslsocketv6::connect()
{
	if(!_connect())
	{
		std::cerr << "Error Connecting" << std::endl;
		return false;
	}
	if(!SSL_set_fd(_ssl,_descriptsocket))
	{
		std::cerr << "Error set dectriptor socket as SSL" << std::endl;
		return false;
	}
	if(SSL_connect(_ssl) != 1)
	{
		std::cerr << "Error SSL connect" << std::endl;
		return false;
	}
	std::cout << "Connection is secured" << std::endl;
	return true;
}

ssize_t sslsocketv6::send(std::string mess)
{
	if(mess.size())
	{
		int err = SSL_write(_ssl,mess.c_str(),mess.size());
		if(err < 0)
		{
			std::cerr << "Error SSL write error" << (err = SSL_get_error(_ssl,err)) << std::endl;
		}
		return err;
	}
	std::cerr << "Please write request" << std::endl;
	return -1;
}

std::string sslsocketv6::recv()
{
	char buf[BUF_SIZE+1];
	int answersz = 1;
	std::string answer;
	while (answersz = SSL_read(_ssl,buf,BUF_SIZE))
	{
		buf[answersz] = '\0';
		answer += buf;
	}
	if(answersz <= 0)
		{
			std::cerr << "Error bad server answer" << std::endl;
			return answer;
		}
	return answer = buf;
}


sslsocketv6::~sslsocketv6()
{
	close();
	ERR_free_strings();
	EVP_cleanup();
}

