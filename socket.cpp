/*
 * socket.cpp
 *
 *  Created on: 18 июн. 2018 г.
 *      Author: jun
 */



#include "socket.h"


#include <iostream>
#include <string.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

socket::socket()
{
}

socket::~socket()
{
	//close socket. using close(int filedescriptor)
	if(_descriptsocket >= 0)
		close(_descriptsocket);
}

socket::socket(const char* host,uint16_t port)
:_host(host), _port(port), _error(true)
{
	_addr.sin_family = AF_INET;
	_addr.sin_port = htons(port);
	_addr.sin_addr.s_addr = inet_addr(_host.c_str());
	// function inet_addr(255.255.255.255) return -1
	if(_addr.sin_addr.s_addr >= 0 || _host != "255.255.255.255"){
		if((_descriptsocket = ::socket(PF_INET,SOCK_STREAM,0)) < 0)
			{
				std::cerr << "Error socket descriptor" << std::endl;
				_error = false;
			}
	}else {
		std::cerr << "Invalid IP addres" << std::endl;
		_error = false;
	}
}

inline bool socket::status()
{
	return _error;
}

bool socket::connect()
{
	std::cout <<"Connected... ";
	if(::connect(_descriptsocket,(struct sockaddr *) &_addr, sizeof(_addr)) < 0)
	{
		std::cerr << "Error connect to " << _host << ':' << _port << std::endl;
		return false;
	}
	std::cout << " Complete." << std::endl;
	return true;
}

ssize_t socket::send(std::string request)
{
	ssize_t size;
	if(request.empty())
	{
		std::cerr << "Please write request" << std::endl;
		return -1;
	}
	if((size = ::send(_descriptsocket, request.c_str(), request.size(),0)) != request.size())
	{
		if(size < 0)
		{
			std::cerr << "Error send request" << std::endl;
			return size;
		}
		std::cout << "Send " << size << " bytes from " << request.size() << std::endl;
	}


	return size;
}

std::string socket::recv()
{
	char buf[BUF_SIZE+1];
	int answersz = 1;
	std::string answer;
	while((answersz = ::recv(_descriptsocket,buf,BUF_SIZE,0)) > 0)
	{
		buf[answersz] = '\0';
		answer+=buf;
	}
	if(answersz < 0)
	{
		std::cerr << "Error bad server answer" << std::endl;
		return answer += "Error";
	}
	if(answer.empty())
	{
		std::cout << "Server Answer is \"\"" <<std::endl;
	}
	return answer;
}



























