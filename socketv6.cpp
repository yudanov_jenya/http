/*
 * socket.cpp
 *
 *  Created on: 18 июн. 2018 г.
 *      Author: jun
 */



#include "socketv6.h"


#include <iostream>
#include <string.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>

socketv6::socketv6()
{
}

socketv6::~socketv6()
{
	//close socket. using close(int filedescriptor)
	if(_descriptsocket >= 0)
		close(_descriptsocket);
}

socketv6::socketv6(const char* host,uint16_t port)
:_host(host),_port(port),_descriptsocket(-1),_result(NULL),_error(true)
{
	memset(&_addr, 0, sizeof(_addr));
	_addr.ai_family = AF_UNSPEC;
	_addr.ai_socktype = SOCK_STREAM;
	_addr.ai_flags = AI_PASSIVE;

	char portch[10];
	sprintf(portch, "%hu", _port);

	if(getaddrinfo(_host.c_str(), portch, &_addr, &_result) != 0 || !_result)
	{
		std::cout << "Host not found: " << _host << std::endl;
		_error = false;
	}
}

inline bool socketv6::status()
{
	return _error;
}


bool socketv6::_connect()
{
	struct addrinfo *res = NULL;
		res = _result;
		while (res)
		{
			fprintf(stdout, "Found host: socktype=%d, protocol=%d, family=%d\n",
					res->ai_socktype,
					res->ai_protocol,
					res->ai_family);

			_descriptsocket = socket(res->ai_family,
						  res->ai_socktype,
						  res->ai_protocol);
			if(_descriptsocket >= 0)
			{
				if (::connect(_descriptsocket, res->ai_addr, res->ai_addrlen) == 0)
				{
					fprintf(stdout, "Connected with socktype=%d, protocol=%d, family=%d\n",
							res->ai_socktype,
							res->ai_protocol,
							res->ai_family);
					freeaddrinfo(_result);
					return true;
				}
				close(_descriptsocket);
				_descriptsocket = -1;
			}
			res = res->ai_next;
		}
		freeaddrinfo(_result);
		return false;
}


bool socketv6::connect()
{
	if(_connect())
	{
		return true;
	}
	std::cerr << "Error Connecting." << std::endl;
	return false;
}

ssize_t socketv6::send(std::string request)
{
	ssize_t size;
	if(request.empty())
	{
		std::cout << "Please write request" << std::endl;
		return -1;
	}
	if((size = ::send(_descriptsocket, request.c_str(), request.size(),0)) != request.size())
	{
		if(size < 0)
		{
			std::cerr << "Error send request" << std::endl;
			return size;
		}
		std::cout << "Send " << size << " bytes from " << request.size() << std::endl;
	}


	return size;
}

std::string socketv6::recv()
{
	char buf[BUF_SIZE+1];
	int answersz = 1;
	std::string answer;
	while((answersz = ::recv(_descriptsocket,buf,BUF_SIZE,0)) > 0)
	{
		buf[answersz] = '\0';
		answer+=buf;
	}
	if(answersz < 0)
	{
		std::cerr << "Error bad server answer" << std::endl;
		return NULL;
	}
	if(answer.empty())
	{
		std::cout << "Server Answer is \"\"" <<std::endl;
	}
	return answer;
}


























